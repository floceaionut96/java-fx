package LoginGUI;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;


public class Login extends Application {

    private static List persons=new ArrayList();

    @Override
    public void start(Stage primaryStage) {

        GridPane pane=new GridPane();
        pane.setPadding(new Insets(20,20,20,20));
        pane.setVgap(10);
        pane.setHgap(6);

        /*Button test=new Button("test");
        test.setOnAction(e->ErrorPage.displayErrorPage("Username should contain only letters."));
        GridPane.setConstraints(test,4,7);*/

        Label loginName=new Label("Username:");
        TextField userField= new TextField();
        userField.setPromptText("username");
        GridPane.setConstraints(loginName,4,1);
        GridPane.setConstraints(userField,5,1);

        Label password=new Label("Password:");
        TextField passwordField=new TextField();
        passwordField.setPromptText("password");
        GridPane.setConstraints(password,4,3);
        GridPane.setConstraints(passwordField,5,3);

        Button login=new Button("Login");
        login.setPrefSize(60,10);
        Button  register=new Button("Register");
        register.setOnAction(e-> RegisterPage.displayRegisterPage());
        GridPane.setConstraints(login,5,4);
        GridPane.setConstraints(register,5,5);



        pane.getChildren().addAll(loginName,userField,password,passwordField,login,register);

        Scene scene=new Scene(pane,300,200);
        primaryStage.setTitle("Login");
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static void launchApp(String[] args){
        launch(args);
    }

    public static List getPersons() {
        return persons;
    }
}
