package LoginGUI;

import javafx.scene.paint.Color;

import java.sql.*;
import java.util.List;

public class PersonDAO {
    private static final String INSERT="INSERT INTO persons ( `first_name` , `last_name` , `gender` , `E-mail` , `username` , `password` ) VALUES (?,?,?,?,?,?)";
    private static final String SELECT_BY_USERNAME="SELECT * FROM persons WHERE username=?";

    public static Connection getConn() throws SQLException {
        String fileURL = "jdbc:mysql://remotemysql.com:3306/b0LzVzq3Xf";
        String host = "remotemysql.com";
        String user = "b0LzVzq3Xf";
        String pass = "M7OzJsQrGi";
        String DBname = "b0LzVzq3Xf";

        Connection conn = DriverManager.getConnection(fileURL, user, pass);
        return conn;
    }

    public static void insertPerson(PersonDTO p) throws SQLException {
        try(Connection c=getConn();
            PreparedStatement ps= c.prepareStatement(INSERT)){
            ps.setString(1,upperFirstLetter(p.getFirstName()));
            ps.setString(2,upperFirstLetter(p.getLastName()));
            ps.setString(3,p.getGender().toString());
            ps.setString(4,p.getEmail());
            ps.setString(5,p.getUsername());
            ps.setString(6,p.getPassword());

            int rowNO=ps.executeUpdate();

            if(rowNO==1) {
                ErrorPage.displayErrorPage("Account created successfully!", Color.GREEN);
            }
            System.out.println("Inserted "+ rowNO+" rows");
        }
    }


    public static Boolean usernameExists(String username){
        try(Connection c=getConn();
            PreparedStatement ps= c.prepareStatement(SELECT_BY_USERNAME))
            {
            ps.setString(1,username);
            ResultSet rs= ps.executeQuery();
            while (rs.next()){
                String str=rs.getString("username");
                System.out.println("FOUND USERNAME "+str);
                if(username.equalsIgnoreCase(str)){
                    return true;
                }
            }
        } catch (SQLException throwables) {
            return false;
        }
        return false;
    }

    public static String upperFirstLetter(String str){
        return str.substring(0,1).toUpperCase()+str.substring(1).toLowerCase();
    }
}
