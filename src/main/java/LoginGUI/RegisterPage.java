package LoginGUI;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.security.spec.RSAOtherPrimeInfo;
import java.sql.SQLException;


public class RegisterPage {

    private static boolean close=false;

    public static void displayRegisterPage(){

        GridPane pane=new GridPane();
        pane.setPadding(new Insets(20,20,20,20));
        pane.setHgap(10);
        pane.setVgap(8);

        Label firstName = new Label("First name:");
        pane.setConstraints(firstName,1,1);
        TextField firstNameField=new TextField();
        Label lastName = new Label("Last name:");
        TextField lastNameField=new TextField();
        GridPane.setConstraints(lastNameField,2,2);
        Label gender=new Label("Gender:");
        Label email=new Label("E-mail:");
        TextField emailField=new TextField();
        Label username = new Label("Username:");
        TextField usernameField=new TextField();
        Label password=new Label("Password:");
        PasswordField passwordField=new PasswordField();
        Label passwordAgain =new Label("Password again:");
        PasswordField passwordAgainField=new PasswordField();
        Button register=new Button("Register");

        ToggleGroup genderToggleGroup=new ToggleGroup();
        RadioButton maleBtn=new RadioButton("Male");
        maleBtn.setToggleGroup(genderToggleGroup);
        RadioButton femaleBtn=new RadioButton("Female");
        femaleBtn.setToggleGroup(genderToggleGroup);

        HBox genderRadio=new HBox();
        genderRadio.setSpacing(15);
        genderRadio.getChildren().addAll(maleBtn,femaleBtn);

        register.setOnAction(e-> {
            if(isOnlyLetters(firstNameField.getText())==false || isOnlyLetters(lastNameField.getText())==false){
                ErrorPage.displayErrorPage("Name fields should contain only letters!", Color.RED);
                return;
            }
            else if(isValidEmail(emailField.getText())==false){
                ErrorPage.displayErrorPage("Check the E-mail field again!",Color.RED);
                return;
            }
            else if(!passwordField.getText().equals(passwordAgainField.getText())){
                ErrorPage.displayErrorPage("Passwords doesn't match!",Color.RED);
                return;
            }
            else if(PersonDAO.usernameExists(usernameField.getText())==true){
                ErrorPage.displayErrorPage("This username is already taken!",Color.RED);
                return;
            }
            PersonDTO personDTO=new PersonDTO(firstNameField.getText(),lastNameField.getText(),toEnum(((RadioButton)genderToggleGroup.getSelectedToggle()).getText()),emailField.getText()
            ,usernameField.getText(),passwordField.getText());
            try {
                PersonDAO.insertPerson(personDTO);
            } catch (SQLException throwables) {
                System.out.println("PERSONDAO ERROR");;
            }
        });
        Button cancel=new Button("Cancel");


        pane.setConstraints(firstName,1,1);
        pane.setConstraints(firstNameField,2,1);
        pane.setConstraints(lastName,1,2);
        GridPane.setConstraints(lastNameField,2,2);
        pane.add(gender,1,3);
        pane.add(genderRadio,2,3);
        GridPane.setConstraints(email,1,4);
        GridPane.setConstraints(emailField,2,4);
        GridPane.setConstraints(username,1,5);
        GridPane.setConstraints(usernameField,2,5);
        GridPane.setConstraints(password,1,6);
        GridPane.setConstraints(passwordField,2,6);
        GridPane.setConstraints(passwordAgain,1,7);
        GridPane.setConstraints(passwordAgainField,2,7);
        GridPane.setConstraints(register,1,8);
        GridPane.setConstraints(cancel,2,8);

        pane.getChildren().addAll(firstName,firstNameField,lastName,lastNameField,email,
        emailField,username,usernameField,password,passwordField,passwordAgain,passwordAgainField,register,cancel);

        StackPane stack=new StackPane();
        stack.getChildren().add(pane);

        Scene scene =new Scene(stack,300,300);

        Stage registerStage=new Stage();
        registerStage.initModality(Modality.APPLICATION_MODAL);
        registerStage.setScene(scene);
        registerStage.setTitle("Register");
        registerStage.show();
        cancel.setOnAction(e->registerStage.close());

        /*if(close=true){
            registerStage.close();
            close=false;
        }*/

    }

    public static PersonDTO createPerson(String firstname, String lastname,Gender gender, String email, String username, String password){
        PersonDTO person=new PersonDTO(firstname,lastname,gender,email,username,password);
        Login.getPersons().add(person);
        return person;
    }

    public static boolean isClose() {
        return close;
    }

    public static void setClose(boolean close) {
        RegisterPage.close = close;
    }

    public static Gender toEnum(String gender){
        if(gender.equals("Male")){
            return Gender.MALE;
        }
        else if(gender.equals("Female")){
            return Gender.FEMALE;
        }
        else{
            return null;
        }
    }

    public static boolean isOnlyLetters(String input){
        char[] chars=input.toCharArray();

        for(char c: chars){
            if(!Character.isLetter(c)){
                return false;
            }
        }
        return true;
    }

    public static boolean isValidEmail(String input) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return input.matches(regex);
    }

}
