package JVX;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.Optional;

public class Main extends Application {
    Scene scene1;
    Scene scene2;
    Scene scene3;

    @Override
    public void start(Stage primaryStage) {

        Button button1 =new Button("To Scene 2");
        button1.setOnAction(e->primaryStage.setScene(scene2));
        Label label1 =new Label("This is Scene 1");
        label1.setTextFill(Color.WHITE);
        Button buttonChange=new Button("Change");
        buttonChange.setOnAction(e-> ChangeText(label1));

        label1.setFont(new Font("Arial",30));
        VBox root1 = new VBox(40);
        root1.setBackground(new Background(new BackgroundFill(Color.BLUE, CornerRadii.EMPTY, Insets.EMPTY)));
        root1.getChildren().addAll(label1,button1,buttonChange);
        scene1 =new Scene(root1,500,380);


        Button button2 =new Button("To scene 1");
        button2.setOnAction(e->primaryStage.setScene(scene1));
        Button button21 =new Button("To scene 3");
        button21.setOnAction(e-> primaryStage.setScene(scene3));
        Label label2 =new Label("This is scene 2");
        label2.setFont(new Font("Arial",30));
        VBox root2=new VBox(40);
        root2.getChildren().addAll(label2,button2,button21);
        scene2 = new Scene(root2,500,380);

        Button button3=new Button("To scene 2");
        button3.setOnAction(e->primaryStage.setScene(scene2));
        Button button31=new Button("Exit Application");
        button31.setOnAction(e->primaryStage.hide());
        Label label3 =new Label("This is scene 3");
        label3.setFont(new Font("Arial",30));
        VBox root3=new VBox(40);
        root3.getChildren().addAll(label3,button3,button31);
        scene3=new Scene(root3,500,380);

        primaryStage.setTitle("My FX App");
        primaryStage.setScene(scene1);
        primaryStage.show();
    }

    public static void launchApp(String[] args){
        launch(args);
    }

    private static void ChangeText(Label label){
        if(label.getText().equals("This is another text")){
            label.setText("You changed it again");
        }
        else{
            label.setText("This is another text");
        }

    }
}
