package LoginGUI;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;


public class ConfirmationPage {

    public static void displayConfirmationPage(String user){
        Stage confirmation=new Stage();

        Label label=new Label("The user "+user+" has been registered!");
        Button thanksButton=new Button("Thanks!");
        thanksButton.setOnAction(e->{
            confirmation.hide();
            RegisterPage.setClose(true);
        });

        VBox vbox=new VBox(10);
        vbox.getChildren().addAll(label,thanksButton);
        StackPane root=new StackPane();
        root.getChildren().add(vbox);

        Scene scene=new Scene(root,300,300);

        confirmation.setScene(scene);
        confirmation.show();
    }
}
