package LoginGUI;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.util.Stack;


public class ErrorPage {

    public static void displayErrorPage(String message,Color color){

        StackPane root2=new StackPane();
        GridPane root=new GridPane();
        root.setVgap(20);
        root.setHgap(20);
        Label text=new Label(message);
        text.setTextFill(color);
        text.setFont(new Font(17));
        StackPane stackpane2=new StackPane();
        stackpane2.getChildren().add(text);
        StackPane stackpane=new StackPane();
        Button okbutton=new Button("OK!");
        stackpane.getChildren().add(okbutton);
        GridPane.setConstraints(stackpane2,2,2);
        GridPane.setConstraints(stackpane,2,3);
        root.getChildren().addAll(stackpane2,stackpane);
        root2.getChildren().add(root);


        Scene scene = new Scene(root2,345,160);
        Stage errorPage=new Stage();
        errorPage.setScene(scene);
        errorPage.initModality(Modality.APPLICATION_MODAL);
        errorPage.show();
        okbutton.setOnAction(e->errorPage.close());

    }

}
